#!/usr/bin/env bash
# Main Entry Script for creating Odroid xu image
# See build_xu_img.sh

#ELEVATE to root
if (( UID != 0 )); then
    exec sudo -E "$0" ${1+"$@"}
fi

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'

usage="${BLUE} usage: ./build_xu.sh <img file name> <path to bl files> <path to git project> <target's root block device >${NC}\n
${BLUE}example: ./build_xu.sh my_xu3.img /mnt/Secondary/git/u-boot/sd_fuse/ /mnt/Secondary/git/linux/ /dev/mmcblk0p2${NC}"

if [ -z $1 ]
then
    echo -e "${RED}Missing image file${NC}"
    echo -e $usage 
    exit 1
fi

if [ -z $2 ]
then
    echo -e "${RED}Missing path to bl files${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $3 ]
then
    echo -e "${RED}Missing path to kernel${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $4 ]
then
    echo -e "${RED}Missing target's root block device ${NC}"
    echo -e $usage
    exit 1
fi

img_name=$1 # my_xu.img
bl_files=$2 # /home/guster32/git/u-boot/sd_fuse/hardkernel/
kernel_git=$3   # /home/guster32/git/odroidxu3-3.10.y/
root_blk_device=$4  # 1024
uboot_size_mb=2
boot_size_mb=100
image_size_mb=1024

debian_arch=armhf
debian_dist=jessie
mount_boot=mnt/boot/
mount_root=mnt/root/


## NEED to stop script if this fails
./build_xu_img.sh $img_name $bl_files $image_size_mb $uboot_size_mb $boot_size_mb

### NEED to stop script if this fails
./build_xu_losetup.sh $img_name $uboot_size_mb $boot_size_mb


### NEED to check if not mounted already then just use that
echo -e "${BLUE}Creating mount points${NC}"
rm -Rf $mount_boot
rm -Rf $mount_root
mkdir -p $mount_boot
mkdir -p $mount_root

echo -e "${BLUE}Mounting the partitions${NC}"
sudo mount /dev/loop36 $mount_boot -o rw,uid=1000,gid=1000,fmask=0022,dmask=0022
sudo mount /dev/loop37 $mount_root -o rw

# Debootstrap
./build_xu_debootstrap.sh $debian_arch $debian_dist $mount_root

# Install Kernel and boot image on partions
blkid=$(sudo blkid /dev/loop37)
rootuuid=`echo $blkid | awk -F\" '{print $2}'`
./build_xu_kernel.sh $kernel_git $mount_boot $mount_root $rootuuid
sync

echo -e "${BLUE}Unmount boot and root${NC}"
sudo umount mnt/boot
sudo umount mnt/root 

echo -e "${BLUE}Detach loop devices${NC}"
sudo losetup -d /dev/loop35
sudo losetup -d /dev/loop36
sudo losetup -d /dev/loop37

echo -e "${GREEN}DONE now simply dd the image to your sd card: ${NC}"
echo -e "${GREEN}sudo dd if=${img_name} of=/dev/mmcblk0${NC}"