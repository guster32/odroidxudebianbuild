#!/usr/bin/env bash
# Create Base Image with 2 partitions and SDFuse bootloader files
# See 'Create disk images'                	https://adayinthelifeof.nl/2011/10/11/creating-partitioned-virtual-disk-images/
# See 'Cross bootstrap rootfs'            	https://wiki.debian.org/EmDebian/CrossDebootstrap
# See 'Properly install boot on odroid u4'	https://blog.mthode.org/posts/2014/Aug/gentoo-on-the-odroid-u3/
# See 'Building U-boot for Odroid u4'     	http://odroid.com/dokuwiki/doku.php?id=en:xu3_building_u-boot
# See 'Building kernel for odroid u4' 		http://odroid.com/dokuwiki/doku.php?id=en:xu3_building_kernel

#ELEVATE to root
if (( UID != 0 )); then
	exec sudo -E "$0" ${1+"$@"}
fi

signed_bl1_position=1
bl2_position=31
uboot_position=63
tzsw_position=719
env_position=1231

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'

usage="${BLUE} usage: ./build_xu_img.sh <img file name> <path to bl files> <imge size MB> <uboot size MB> <boot size MB>${NC}\n
${BLUE}example: ./build_xu_img.sh my_xu3.img /home/guster32/git/u-boot/sd_fuse/ 1024 2 100${NC}"

if [ -z $1 ]
then
	echo -e "${RED}Missing image file${NC}"
    echo -e $usage 
    exit 1
fi

if [ -z $2 ]
then
	echo -e "${RED}Missing path to bl files${NC}"
	echo -e $usage
    exit 1
fi

if [ -z $3 ]
then
	echo -e "${RED}Mising image size${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $4 ]
then
	echo -e "${RED}Missing u-boot size${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $5 ]
then
	echo -e "${RED}Missing boot partition size${NC}"
    echo -e $usage
    exit 1
fi

img_name=$1 # my_xu.img
bl_files=$2 # /home/guster32/git/u-boot/sd_fuse/
image_size_mb=$3 #4096
uboot_size_mb=$4 #2
boot_size_mb=$5  #100

BL1=$bl_files/bl1.bin.hardkernel
BL2=$bl_files/bl2.bin.hardkernel.720k_uboot
UBOOT=$bl_files/../u-boot-dtb.bin
TZSW=$bl_files/tzsw.bin.hardkernel

if [ -f ${img_name} ]; then
    echo -e "${YELLOW}Skiping building base image since ${img_name} already exists${NC}"
    exit 0
fi

if [ ! -f ${BL1} ]; then
    echo -e "${RED} Could not locate BL1: ${BL1} ${NC}"
    exit 1
fi

if [ ! -f ${BL2} ]; then
    echo -e "${RED} Could not locate BL2: ${BL1} ${NC}"
    exit 1
fi

if [ ! -f ${UBOOT} ]; then
    echo -e "${RED} Could not locate UBOOT: ${UBOOT} ${NC}"
    exit 1
fi

if [ ! -f ${TZSW} ]; then
    echo -e "${RED} Could not locate Trusted Zone Sw: ${TZSW} ${NC}"
    exit 1
fi

rm -f ${img_name}_temp

echo -e "${BLUE}Creating Image File ${NC}"

img_size_bytes=$((($image_size_mb * 1024 ) * 1024))
uboot_size_bytes=$((($uboot_size_mb * 1024 ) * 1024))
boot_size_bytes=$((($boot_size_mb * 1024 ) * 1024))

boot_blk_size=$(($boot_size_bytes / 512))

boot_blk_start=$((($uboot_size_bytes / 512) + 1))
root_blk_start=$((($boot_blk_start + $boot_blk_size) + 1))

#Create a 1 GB Disk called ${img_name}_temp
dd if=/dev/zero of=${img_name}_temp bs=512 count=$(($img_size_bytes / 512))

sync
sync

# to create the partitions programatically (rather than manually)
# we're going to simulate the manual input to fdisk
# The sed script strips off all the comments so that we can 
# document what we're doing in-line with the actual commands
# Note that a blank line (commented as "defualt" will send a empty
# line terminated with a newline to take the fdisk default.
sed -e 's/\t\([\+0-9a-zA-Z]*\)[ \t].*/\1/' << EOF | fdisk ${img_name}_temp
	n 					# new partition
	p 					# primary partition
	1 					# partition number 1
	$boot_blk_start 	# Leave space for Uboot 
	+${boot_size_mb}M 	# 100 MB boot parttion
	t 					# Set partion type
	c 					# c for Fat W95 (LBA)
	n 					# new partition
	p 					# primary partition
	2 					# partion number 2
	$root_blk_start 	# Start where the other partition leftoff
	 					# default, extend partition to end of disk
	p 					# print the in-memory partition table
	w 					# write the partition table
	q 					# and we're done
EOF
# Device     Boot  Start     End Sectors   Size Id Type
# my_xu.img1        4096  208895  204800   100M  c W95 FAT32 (LBA)
# my_xu.img2      208896 1953124 1744229 851.7M 83 Linux


./build_xu_losetup.sh ${img_name}_temp $uboot_size_mb $boot_size_mb

echo -e "${BLUE}Formating partitions${NC}"
sudo mkfs.fat /dev/loop36
sudo mkfs.ext4 /dev/loop37

#<BL1 fusing>
echo -e "${BLUE}BL1 fusing${NC}"
# dd if=/home/guster32/git/u-boot/sd_fuse/hardkernel/bl1.bin.hardkernel of=/dev/loop0 seek=$signed_bl1_position
dd if=$BL1 of=/dev/loop35 seek=$signed_bl1_position

#<BL2 fusing>
echo -e "${BLUE}BL2 fusing${NC}"
#dd if=/home/guster32/git/u-boot/sd_fuse/hardkernel/bl2.bin.hardkernel of=/dev/loop0 seek=$bl2_position
dd if=$BL2 of=/dev/loop35 seek=$bl2_position

#<u-boot fusing>
echo -e "${BLUE}u-boot fusing${NC}"
#dd if=/home/guster32/git/u-boot/sd_fuse/hardkernel/u-boot.bin.hardkernel of=/dev/loop0 seek=$uboot_position # Original u-boot
dd if=$UBOOT of=/dev/loop35 seek=$uboot_position

#<TrustZone S/W fusing>
echo -e "${BLUE}TrustZone S/W fusing${NC}"
dd if=$TZSW of=/dev/loop35 seek=$tzsw_position

#<u-boot env default>
echo -e "${BLUE}u-boot env erase${NC}"
dd if=/dev/zero of=/dev/loop35 seek=$env_position count=32 bs=512

echo -e "${BLUE}Detach loop devices${NC}"
sudo losetup -d /dev/loop35
sudo losetup -d /dev/loop36
sudo losetup -d /dev/loop37

mv ${img_name}_temp $img_name

echo -e "${BLUE}DONE Building img file${NC}"