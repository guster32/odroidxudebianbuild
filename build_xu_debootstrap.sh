#!/usr/bin/env bash
# Script for running deboostrap on an image.
# See build_xu.sh

#ELEVATE to root
if (( UID != 0 )); then
    exec sudo -E "$0" ${1+"$@"}
fi

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'

usage="${BLUE} usage: ./build_xu_deboostrap.sh <architecture> <distro version> <mount point>${NC}\n
${BLUE}example: ./build_xu_deboostrap.sh armhf jessie mnt/root/{NC}"

if [ -z $1 ]
then
	echo -e "${RED}Missing architecture${NC}"
    echo -e $usage 
    exit 1
fi

if [ -z $2 ]
then
	echo -e "${RED}Missing distro release${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $3 ]
then
	echo -e "${RED}Missing relative path to root mount point${NC}"
    echo -e $usage
    exit 1
fi

debian_arch=$1
debian_dist=$2
mount_root=$3
debian_site=http://ftp.debian.org/debian/


echo -e "${BLUE}Run debootstrap $debian_arch $debian_dist${NC}"
sudo debootstrap --include=initramfs-tools,wireless-tools,wpasupplicant,u-boot-tools,plymouth,plymouth-themes,xorg,pulseaudio,fcitx --foreign --arch $debian_arch $debian_dist $mount_root $debian_site

#Copy 'qemu-arm-static' to the target file system. To be able to chroot into a target file system, the qemu emulator for the target CPU needs to be accessible from inside the chroot jail.
echo -e "${BLUE}Copy 'qemu-arm-static' to the target file system.${NC}"
sudo cp /usr/bin/qemu-arm-static $mount_root/usr/bin

echo -e "${BLUE}Run the second stage debootstrap${NC}"
sudo DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C chroot $mount_root /debootstrap/debootstrap --second-stage

echo -e "${BLUE}Trigger post install scripts${NC}"
sudo DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C chroot $mount_root dpkg --configure -a

echo -e "${BLUE}Set root password${NC}"
sudo chroot $mount_root sh -c "echo \"root:root\" | chpasswd"
sudo chroot $mount_root sh -c "adduser --disabled-password --gecos \"Debian\" debian"
sudo chroot $mount_root sh -c "echo \"debian:debian\" | chpasswd"
