**build_xu.sh** is bash script that creates a img file from scratch with a bare bone install of debian for odroid xu4. It uses a previously built kernel from [hardkernel linux builds](https://github.com/hardkernel/linux.git) and u-boot from [hardkernel u-boot](https://github.com/hardkernel/u-boot.git) and perfoms a cross debootstrap.

# Requirements:
QEMU
Pre-built Kernel from [hardkernel linux builds](https://github.com/hardkernel/linux.git). Tested with branch (odroidxu3-3.10.y)
Pre-buitl U-boot from [hardkernel u-boot](https://github.com/hardkernel/u-boot.git). Tested with branch (odroidxu3-v2012.07)


# usage:
./build_xu.sh <img file name> <path to bl files> <path to git project> <target's root block device >

#example:

```
#!shell

./build_xu.sh my_xu3.img /mnt/Secondary/git/u-boot/sd_fuse /mnt/Secondary/git/linux /dev/mmcblk0p2


```

sudo apt-get install debootstrap
sudo apt-get install qemu-arm-static
sudo apt-get install qemu
sudo apt-get install u-boot-tools lzop

sync && sudo umount ./mount


Once the script completes simply dd the image to your sd card
Note: The first thing you will want todo once you boot is to create change passwords and perhaps extend the root partition and fs to use all space available on the SDCard
