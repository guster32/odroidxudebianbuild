#!/usr/bin/env bash
# Script for installing kernel and boot images on root and boot partitions
# See build_xu_kernel.sh

#ELEVATE to root
if (( UID != 0 )); then
    exec sudo -E "$0" ${1+"$@"}
fi

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'

usage="${BLUE} usage: ./build_xu_kernel.sh <path to kernel build> <path boot mnt> <path root mnt> <target's root block device >${NC}\n
${BLUE}example: ./build_xu_kernel.sh /mnt/Secondary/git/linux mnt/boot mnt/root /dev/mmcblk0p2${NC}"

if [ -z $1 ]
then
    echo -e "${RED}Missing path to kernel build${NC}"
    echo -e $usage 
    exit 1
fi

if [ -z $2 ]
then
    echo -e "${RED}Missing path to boot mount${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $3 ]
then
    echo -e "${RED}Missing path to root mount${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $4 ]
then
    echo -e "${RED}Missing target's root block device ${NC}"
    echo -e $usage
    exit 1
fi

kernel_git=$1
mount_boot=$2
mount_root=$3
root_blk_device=$4

echo -e "${BLUE}Copy kernel Image and dtb files to boot the partition${NC}"
sudo cp $kernel_git/arch/arm/boot/zImage $kernel_git/arch/arm/boot/dts/exynos5422-odroidxu?.dtb $mount_boot
#sudo cp $kernel_git/arch/arm/boot/dts/exynos5422-odroidxu4.dtb $mount_boot
#sudo cp $kernel_git/arch/arm/boot/zImage $mount_boot/zImage
# pushd $mount_boot
# cat kernel.raw exynos5422-odroidxu3.dtb > kernel
# popd

pushd $mount_root
root_path=$(pwd)
popd

echo -e "${BLUE}Install modules on root partition${NC}"
pushd $kernel_git
release=`make kernelrelease`
sudo make modules_install ARCH=arm INSTALL_MOD_PATH=$root_path
echo -e "${BLUE}Make Image with initramfs${NC}"
sudo chroot $root_path sh -c "update-initramfs -c -k ${release}"
sudo chroot $root_path sh -c "mkimage -A arm -O linux -T ramdisk -C none -a 0 -e 0 -n uInitrd -d /boot/initrd.img-${release} /boot/uInitrd-${release}"
popd
sudo cp ${mount_root}boot/uInitrd-${release} $mount_boot/uInitrd

# #### TODO THIS NEEDS SOME WORK###
echo -e "${BLUE}Create boot.ini file${NC}"
echo -e 'ODROIDXU-UBOOT-CONFIG\n'\
'# U-Boot Parameters\n'\
'setenv initrd_high "0xffffffff"\n'\
'setenv fdt_high "0xffffffff"\n'\
'setenv macaddr "00:1e:06:61:7a:39"\n'\
'setenv vout "hdmi"\n'\
'setenv cecenable "false" # false or true\n'\
'setenv disable_vu7 "true" # false\n'\
'setenv ddr_freq 900\n'\
'setenv governor "performance"\n'\
'setenv external_watchdog "false"\n'\
'setenv hdmi_tx_amp_lvl "31"\n'\
'setenv hdmi_tx_lvl_ch0 "3"\n'\
'setenv hdmi_tx_lvl_ch1 "3"\n'\
'setenv hdmi_tx_lvl_ch2 "3"\n'\
'setenv hdmi_tx_emp_lvl "6"\n'\
'setenv hdmi_clk_amp_lvl "31"\n'\
'setenv hdmi_tx_res "0"\n'\
'setenv external_watchdog_debounce "3"\n'\
'setenv HPD "true"\n'\
"#vt.global_cursor_default=0\n"\
'setenv bootrootfs "consoleblank=0 console=ttySAC2,115200n8 root=UUID='${root_blk_device}' rootwait rw"\n'\
'fatload mmc 0:1 0x40008000 zImage\n'\
'fatload mmc 0:1 0x42000000 uInitrd\n'\
'setenv fdtloaded "false"\n'\
'if test "x${board_name}" = "x"; then setenv board_name "xu4"; fi\n'\
'if test "${board_name}" = "xu4"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu4.dtb; setenv fdtloaded "true"; fi\n'\
'if test "${board_name}" = "xu3"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu3.dtb; setenv fdtloaded "true"; fi\n'\
'if test "${board_name}" = "xu3l"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu3-lite.dtb; setenv fdtloaded "true"; fi\n'\
'if test "${fdtloaded}" = "false"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu4.dtb; setenv fdtloaded "true"; fi\n'\
'fdt addr 0x44000000\n'\
'setenv hdmi_phy_control "hdmi_tx_amp_lvl=${hdmi_tx_amp_lvl} hdmi_tx_lvl_ch0=${hdmi_tx_lvl_ch0} hdmi_tx_lvl_ch1=${hdmi_tx_lvl_ch1} hdmi_tx_lvl_ch2=${hdmi_tx_lvl_ch2} hdmi_tx_emp_lvl=${hdmi_tx_emp_lvl} hdmi_clk_amp_lvl=${hdmi_clk_amp_lvl} hdmi_tx_res=${hdmi_tx_res} HPD=${HPD} vout=${vout}"\n'\
'if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi\n'\
'if test "${disable_vu7}" = "false"; then setenv hid_quirks "usbhid.quirks=0x0eef:0x0005:0x0004"; fi\n'\
'if test "${external_watchdog}" = "true"; then setenv external_watchdog "external_watchdog=${external_watchdog} external_watchdog_debounce=${external_watchdog_debounce}"; fi\n'\
'# final boot args\n'\
'setenv bootargs "${bootrootfs} ${videoconfig} ${hdmi_phy_control} ${hid_quirks} smsc95xx.macaddr=${macaddr} governor=${governor} ${external_watchdog} splash logo.nologo"\n'\
'# set DDR frequency\n'\
'dmc ${ddr_freq}\n'\
'# Boot the board\n'\
'bootz 0x40008000 0x42000000 0x44000000\n'\ > $mount_boot/boot.ini

echo -e "${BLUE}Create fstab${NC}"
echo -e 'UUID='${root_blk_device}' / ext4 errors=remount-ro,noatime 0 1\n' > ${mount_root}etc/fstab

echo -e "${BLUE}Copy overlay${NC}"
rsync --recursive ${mount_root}../../overlay/ $mount_root



# ODROIDXU-UBOOT-CONFIG

# # U-Boot Parameters
# setenv initrd_high "0xffffffff"
# setenv fdt_high "0xffffffff"

# # Mac address configuration
# setenv macaddr "00:1e:06:61:7a:39"

# # --- Screen Configuration for HDMI --- # 
# # ---------------------------------------
# # Uncomment only ONE line! Leave all commented for automatic selection.
# # Uncomment only the setenv line!
# # ---------------------------------------
# # ODROID-VU forced resolution
# # setenv videoconfig "video=HDMI-A-1:1280x800@60"
# # -----------------------------------------------
# # ODROID-VU forced EDID
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1280x800.bin"
# # -----------------------------------------------
# # 1920x1200 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1200_60hz.bin"
# # -----------------------------------------------
# # 1920x1200 30hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1200_30hz.bin"
# # -----------------------------------------------
# # 1920x1080 (1080P) with monitor provided EDID information. (1080p-edid)
# # setenv videoconfig "video=HDMI-A-1:1920x1080@60"
# # -----------------------------------------------
# # 1920x1080 (1080P) without monitor data using generic information (1080p-noedid)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1080.bin"
# # -----------------------------------------------
# # 1920x1080 50hz (1080P) with monitor provided EDID information. (1080p 50hz-edid)
# # setenv videoconfig "video=HDMI-A-1:1920x1080@50"
# # -----------------------------------------------
# # 1920x1080 50hz (1080P) without monitor data using generic information (1080p 50hz-noedid)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1080_50hz.bin"
# # -----------------------------------------------
# # 1920x1080 24Hz (1080P) without monitor data using generic information (1080p 24hz-noedid)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1080_24hz.bin"
# # -----------------------------------------------
# # 1920x1080 23.976Hz (1080P) without monitor data using generic information (1080p 23.976hz-noedid)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x1080_23_976hz.bin"
# # -----------------------------------------------
# # 1920x800 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1920x800.bin"
# # -----------------------------------------------
# # 1792x1344 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1792x1344.bin"
# # -----------------------------------------------
# # 1680x1050 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1680x1050.bin"
# # -----------------------------------------------
# # 1600x1200 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1600x1200.bin"
# # -----------------------------------------------
# # 1600x900 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1600x900.bin"
# # -----------------------------------------------
# # 1400x1050 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1400x1050.bin"
# # -----------------------------------------------
# # 1440x900 with monitor provided EDID information.
# # setenv videoconfig "video=HDMI-A-1:1440x900@60"
# # -----------------------------------------------
# # 1440x900 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1440x900.bin"
# # -----------------------------------------------
# # 1366x768 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1366x768.bin"
# # -----------------------------------------------
# # 1360x768 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1360x768.bin"
# # -----------------------------------------------
# # 1280x1024 without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1280x1024.bin"
# # -----------------------------------------------
# # 1280x768 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1280x768.bin"
# # -----------------------------------------------
# # 1280x720 (720P) with monitor provided EDID information. (720p-edid)
# # setenv videoconfig "video=HDMI-A-1:1280x720@60"
# # -----------------------------------------------
# # 1280x720 (720P) without monitor data using generic information (720p-noedid)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1280x720.bin"
# # -----------------------------------------------
# # 1152x864 75hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1152x864_75hz.bin"
# # -----------------------------------------------
# # 1024x768 without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1024x768.bin"
# # -----------------------------------------------
# # 1024x600 without monitor data using generic information (ODROID VU7+)
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/1024x600.bin"
# # -----------------------------------------------
# # 800x600 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/800x600.bin"
# # -----------------------------------------------
# # 848x480 60hz without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/848x480.bin"
# # -----------------------------------------------
# # 800x480 without monitor data using generic information (ODROID 7")
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/800x480.bin"
# # -----------------------------------------------
# # 720x576 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/720x576.bin"
# # -----------------------------------------------
# # 720x480 without monitor data using generic information 
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/720x480.bin"
# # -----------------------------------------------
# # 640x480 without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/640x480.bin"
# # -----------------------------------------------
# # 480x800 without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/480x800.bin"
# # -----------------------------------------------
# # 480x320 without monitor data using generic information
# # setenv videoconfig "drm_kms_helper.edid_firmware=edid/480x320.bin"

# # --- HDMI / DVI Mode Selection ---
# # ------------------------------------------
# # - HDMI Mode
# setenv vout "hdmi"
# # - DVI Mode (disables sound over HDMI as per DVI compat)
# # setenv vout "dvi"

# # --- HDMI CEC Configuration ---
# # ------------------------------------------
# setenv cecenable "false" # false or true
# # set to true to enable HDMI CEC

# # Enable/Disable ODROID-VU7 Touchsreen
# setenv disable_vu7 "false" # false

# # DRAM Frequency
# # Sets the LPDDR3 memory frequency
# # Supported values: 933 825 728 633 (MHZ)
# setenv ddr_freq 825

# # External watchdog board enable
# setenv external_watchdog "false"
# # debounce time set to 3 ~ 10 sec, default 3 sec
# setenv external_watchdog_debounce "3"


# #------------------------------------------------------------------------------
# #
# # HDMI Hot Plug detection
# #
# #------------------------------------------------------------------------------
# #
# # Forces the HDMI subsystem to ignore the check if the cable is connected or 
# # not.
# # false : disable the detection and force it as connected.
# # true : let cable, board and monitor decide the connection status.
# # 
# # default: true
# # 
# #------------------------------------------------------------------------------
# setenv HPD "true"

# #------------------------------------------------------------------------------------------------------
# # Basic Ubuntu Setup. Don't touch unless you know what you are doing.
# # --------------------------------
# setenv bootrootfs "console=tty1 console=ttySAC2,115200n8 root=UUID=e139ce78-9841-40fe-8823-96a304a09859 rootwait ro fsck.repair=yes net.ifnames=0"


# # Load kernel, initrd and dtb in that sequence
# fatload mmc 0:1 0x40008000 zImage
# fatload mmc 0:1 0x42000000 uInitrd

# setenv fdtloaded "false"
# if test "x${board_name}" = "x"; then setenv board_name "xu4"; fi
# if test "${board_name}" = "xu4"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu4.dtb; setenv fdtloaded "true"; fi
# if test "${board_name}" = "xu3"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu3.dtb; setenv fdtloaded "true"; fi
# if test "${board_name}" = "xu3l"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu3-lite.dtb; setenv fdtloaded "true"; fi
# if test "${fdtloaded}" = "false"; then fatload mmc 0:1 0x44000000 exynos5422-odroidxu4.dtb; setenv fdtloaded "true"; fi

# fdt addr 0x44000000

# setenv hdmi_phy_control "HPD=${HPD} vout=${vout}"
# if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi
# if test "${disable_vu7}" = "false"; then setenv hid_quirks "usbhid.quirks=0x0eef:0x0005:0x0004"; fi
# if test "${external_watchdog}" = "true"; then setenv external_watchdog "external_watchdog=${external_watchdog} external_watchdog_debounce=${external_watchdog_debounce}"; fi

# # final boot args
# setenv bootargs "${bootrootfs} ${videoconfig} ${hdmi_phy_control} ${hid_quirks} smsc95xx.macaddr=${macaddr} ${external_watchdog}"

# # set DDR frequency
# dmc ${ddr_freq}

# # Boot the board
# bootz 0x40008000 0x42000000 0x44000000
