#!/usr/bin/env bash
# Script for creating loopback devices on the image.
# See build_xu.sh
# See build_xu_img.sh

#ELEVATE to root
if (( UID != 0 )); then
    exec sudo -E "$0" ${1+"$@"}
fi

NC='\033[0m' # No Color
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'

usage="${BLUE} usage: ./build_xu_losetup.sh <img file name> <uboot size MB> <boot size MB>${NC}\n
${BLUE}example: ./build_xu_losetup.sh my_xu3.img 2 100${NC}"

if [ -z $1 ]
then
    echo -e "${RED}Missing image file${NC}"
    echo -e $usage 
    exit 1
fi

if [ -z $2 ]
then
    echo -e "${RED}Missing u-boot size${NC}"
    echo -e $usage
    exit 1
fi

if [ -z $3 ]
then
    echo -e "${RED}Missing boot partition size${NC}"
    echo -e $usage
    exit 1
fi

img_name=$1 # my_xu.img
uboot_size_mb=$2 #2
boot_size_mb=$3  #100

uboot_size_bytes=$((($uboot_size_mb * 1024 ) * 1024))
boot_size_bytes=$((($boot_size_mb * 1024 ) * 1024))

boot_blk_size=$(($boot_size_bytes / 512))
boot_blk_start=$((($uboot_size_bytes / 512) + 1))
root_blk_start=$((($boot_blk_start + $boot_blk_size) + 1))

echo -e "${BLUE}Creating loopback device for device${NC}"
sudo losetup --sizelimit $uboot_size_bytes /dev/loop35 ${img_name}

echo -e "${BLUE}Creating loopback device for boot${NC}"
sudo losetup -o $(($boot_blk_start * 512)) --sizelimit $boot_size_bytes /dev/loop36 ${img_name}

echo -e "${BLUE}Creating loopback device for root${NC}"
sudo losetup -o $(($root_blk_start * 512)) /dev/loop37 ${img_name}
